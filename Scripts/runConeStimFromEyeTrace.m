clear all
clc
% close all

D = [5 20]; % diffusion constant arcmin^2/s
Fs = 1000; % sampling rate (Hz)
nSamples = 500; % number of samples
nTraces = 10; % number of traces
numberOfBlobsOG = 525;%runLabelImage('cones2x');
counter = 1;

for d = D
    for ii = 1:nTraces
        figure;
        [Xe, Ye] = EM_Brownian2(5, Fs, nSamples, 1);
        plot(Xe,Ye,'-')
        axis([-7.5 7.5 -7.5 7.5])
        hold on
        [Xe, Ye] = EM_Brownian2(20, Fs, nSamples, 1);
        plot(Xe,Ye,'-')
        axis square
        
        
        size_retina = 319; % pixels, keep it odd
        half_size_retina = floor(size_retina/2);
        
        pixel_angle = .25; % pixel angle = xx arcmin (of images)
        
        % mv = nan(size_retina, size_retina, nSamples);
        [img1, map, alphachannel] = imread('cones2x.png');
        conesOG = double(img1) / 255;
        
        hFigure = figure;
        set(hFigure, 'MenuBar', 'none');
        set(hFigure, 'ToolBar', 'none');figure;
        redChannel = img1(:,:,1);
        greenChannel = img1(:,:,2);
        blueChannel = img1(:,:,3);
        % black pixels are where red channel and green channel and blue channel in an rgb image are 0;
        blackpixelsmask = redChannel <= 50 & greenChannel <= 50 & blueChannel <= 50;
        % show binary image where the black pixels were
        RI = imref2d(size(blackpixelsmask));
        % RI.XWorldLimits = [-3 3];
        % RI.YWorldLimits = [-3 3];
        RI.XWorldLimits = [-6 6];
        RI.YWorldLimits = [-6 6];
        
        imshow(flipud(rgb2gray(img1)), RI, 'Border', 'tight')
        
        hold on
        imgWTrace = plot(Xe, Ye,'-','Color','w','LineWidth',12);
        F = getframe(gcf);
        [X, Map] = frame2im(F);
        axis off
        nameFile = sprintf('CreatedImages/Img%i_dc%i',ii,d);
%         ax = gca;
%         ax.Toolbar.Visible = 'off';
        imwrite(X,sprintf('%s.png',nameFile))
        % saveas(gcf,'CreatedImages/Img1.png');
        numberOfBlobsWithTrace = runLabelImage(nameFile);
        
        numberActivatedCones(ii) = numberOfBlobsOG - numberOfBlobsWithTrace;
    end
    meanActivatedCones(counter) = mean(numberActivatedCones);
    counter = counter + 1;
end
data.activatedCones = meanActivatedCones;
data.dc = D;
data.numRuns = nTraces;
save('dataTraceVsCones.mat', data);
close all
% conesIMG = padarray(conesOG,[(340-length(conesOG))/2, ...
%     (340-length(conesOG))/2],1);

% % % size_img = size(conesOG, 1);
% % %
% % % img = conesOG; % vertical image coordinates are reversed
% % % x_img_arcmin = ((1:size_img) - (size_img + 1) / 2) * pixel_angle; % angular position of each pixel in image
% % %
% % % counter = 1;
% % % for tracei = 1:nTraces
% % %     for ti = 1:nSamples
% % %         x_eye_arcmin = Xe(tracei,ti); % current eye position
% % %         y_eye_arcmin = Ye(tracei,ti);
% % %
% % %         % get eye position in units of image pixels
% % %         x_eye_pixel = interp1(x_img_arcmin, 1:size_img, x_eye_arcmin, 'nearest');
% % %         y_eye_pixel = interp1(x_img_arcmin, 1:size_img, y_eye_arcmin, 'nearest');
% % %
% % %         mv(:, :, counter) = img(...
% % %             y_eye_pixel + (-half_size_retina:half_size_retina),...
% % %             x_eye_pixel + (-half_size_retina:half_size_retina));
% % %         counter = counter + 1;
% % %     end
% % % end
% % %
% % %
% % % % Advanced Matlab Exercise:
% % % %   If you have to do many of these you can speed up computation by making
% % % %   this movie without the use of any for loops.
% % %
% % % playRetinalInputMovie(mv, x_img_arcmin, Xe, Ye, Fs);

