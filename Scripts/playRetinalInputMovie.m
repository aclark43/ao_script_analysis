function playRetinalInputMovie(mv, x_img_arcmin, Xe, Ye, Fs)
% playRetinalInputMovie plays 3d-array mv as a movie in matlab.
% inputs:
%   mv: 3d array [space, space, time]
%   x_img_arcmin: position of each spatial pixel in units (same for horz
%       and vert)
%   Xe, Ye: eye trace
%   fh: figure handle to use
%
% author: Janis Intoy, April 8, 2020

nSamples = length(Xe);

% compute eye velocities and speed
X_vel = sgfilt(Xe, 3, 31, 1);
Y_vel = sgfilt(Ye, 3, 31, 1);

spd = sqrt(X_vel.^2 + Y_vel.^2) / Fs;

mn = quantile(mv(:), 0.05);
mx = quantile(mv(:), 0.95);

% show the movie
figure(); clf;
ax1 = subplot(1, 2, 1);
himg = imagesc(x_img_arcmin, x_img_arcmin, mv(:, :, 1));
axis image; axis off;
colormap gray;
xlabel('horizontal (arcmin)');
ylabel('vertical (arcmin)');
axis tight; 
hold on
% axis([-30 30 -30 30])
axis square


subplot(1, 2, 2); hold on;
plot(Xe, 'k'); plot(Ye, 'Color', [.5 .5 .5]);
yl = ylim;
h_line = plot([0, 0], [-4 4], 'b', 'linewidth', 2);
ylim(yl);
xlim([1, nSamples]);
ylabel('gaze position (arcmin)');
axis square
% subplot(2, 2, 4); hold on;
% plot(spd, 'r');
xlabel('time (ms)');
% ylabel('eye speed deg / s');
% yl = ylim;
% h_line(1, 2) = plot([0, 0], yl, 'b', 'linewidth', 2);
% xlim([1, nSamples]);
% ylim(yl);
ylim([-4 4]);
% caxis(ax1, [mn, mx]);
frame_count = 0;
for ti = 1:2:nSamples
    frame_count = frame_count + 1;
    set(himg, 'CData', mv(:, :, ti));
    set(h_line, 'XData', [ti, ti]);
%     pause(.000009);
    movie_trial(frame_count) = getframe(gcf);
end
videoName = sprintf('VideoOutput');
v_trial = VideoWriter(videoName, 'Motion JPEG AVI');
v_trial.FrameRate = 6;
open(v_trial);
for ii = 1:4:frame_count
    writeVideo(v_trial, movie_trial(ii));
 end
% close(v_trial);

% input ''
% clf

end