% Calculating the cycles/deg frequency for subjects given the data in Wang
% et al 2019 with Austin Roorda
clear all
clc

dataFromWangEtAl.PRL.angular = [12470 11758 11476 11317 13961 14869 8813 11913 ...
    15210 14636 14839 13894 13334 13543 11671 11848 15989 12244 14708 ...
    12449 14060 13787 15287 14409 16562 10876 17481 17899];
dataFromWangEtAl.PRL.mm = [181952 163965 153319 148721 179594 186972 115273 ...
    153566 193824 184921 166278 166422 149334 141033 127480 121826 172286 ...
    131153 147060 128530 144506 142601 155729 140492 148354 97623 154287 154437];

dataFromWangEtAl.PCD.mm = [194625 177692 162890 159027 204020 193090 163676 ...
    158346 196844 189377 174122 179435 153998 145588 150204 132443 179779 ...
    153681 147115 141971 151699 152657 159228 155083 153560 118491 159397 ...
    163731];

dataFromReinigerEtAl.PRL.mm = [210145 196477 214584 210212 217811 212760 ...
    193441 190708 207076 205507 172925 170318 198349 196147 190964 188851 ...
    207763 205894 201831 207682 187061 182418 150790 153715 145870 148503 ...
    176732 183331 158973 169702 162728 158460 171912 179022 163501 165061 ...
    161489 168226 159556 157559 221889];

clarkEtAlFreqDrift = [20.0923300256505,30.5385550883342,15.1991108295293,17.4752840000768,23.1012970008316,16.2975083462064,23.1012970008316,15.1991108295293,18.7381742286039,16.2975083462064];

clarkAODatafrequency.PCD = [61.7880675798639,56.5669216194091,52.9228837460852,50.6355485113638];

clarkAODatafrequency.PLF =[58.8534762835617,57.7926982490542,51.3886183690607,48.5019808618769];
% meanDen = mean(dataFromWangEtAl.angular);
% stdDen = std(dataFromWangEtAl.angular);

% for i = 1:length(dataFromWangEtAl.angular)
%     Nc(i) = (sqrt(3)/2)*dataFromWangEtAl.angular(i)
% end

%%% The nominal Nyquist frequency: calculating the Nyquist limit of a 
%   mosaic that had the same density of cones but whose packing was 
%   triangular where D is the cone density in cones per square millimeter 
%   and 0.291 converts millimeters on the retina to degrees of angular 
%   subtense. This estimate is negligibly different from one based on 
%   direct measurements of the modal frequency of the disordered 
%   extrafoveal mosaic,[6] with the Nyquist frequency defined as half of 
%   the modal frequency.
%   https://www.osapublishing.org/josaa/fulltext.cfm?uri=josaa-4-8-1514&id=2845
for i = 1:length(dataFromWangEtAl.PRL.mm)
    D = dataFromWangEtAl.PRL.mm(i);
    Nf_Wange(i) = (1/2)*(0.291)*((2*D)/(sqrt(3)))^(1/2);
end
for i = 1:length(dataFromWangEtAl.PRL.mm)
    D = dataFromWangEtAl.PCD.mm(i);
    Nf_WangePCD(i) = (1/2)*(0.291)*((2*D)/(sqrt(3)))^(1/2);
end
for i = 1:length(dataFromReinigerEtAl.PRL.mm)
    D = dataFromReinigerEtAl.PRL.mm(i);
    Nf_Rein(i) = (1/2)*(0.291)*((2*D)/(sqrt(3)))^(1/2);
end
figure;
% set(gca,'TickLabelInterpreter','Latex')

y = [{Nf_Wange} {Nf_WangePCD} {Nf_Rein} {clarkAODatafrequency.PCD(1)} ...
    {clarkAODatafrequency.PLF(1)} {clarkEtAlFreqDrift}];
temp = struct2table(cell2struct(y, ...
    {'Wang_PLF','Wang_PCD','Reiniger_PCD','Clark_PCD','Clark_PRL','Clark_Drift'},2));
violinplot(temp,{'Wang_PLF','Wang_PCD','Reiniger_PCD','Clark_PCD','Clark_PRL','Clark_Drift'})
ylabel('Nyquist Limit (cycles/deg)')
xlabel('Paper Datasets')
saveas(gcf,'..\ViolinPlotNyquistFrequency.png');

% violinplot(Nf,{'Cone Density from Wang et al'})%,...
% 
% plot(1:length(Nf),sort(Nf),'-o')
%histogram(Nf, 8)

%% 
figure;
plot(ones(1,length(dataFromWangEtAl.PRL.angular)),dataFromWangEtAl.PRL.angular,'o')
hold on
plot(1, 23930,'d','Color','r','MarkerFaceColor','r');
hold on
plot(1, 12813,'d','Color','m','MarkerFaceColor','m');


